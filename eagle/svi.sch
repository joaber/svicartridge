<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="svi_cartridge">
<packages>
<package name="EDGE_CONNECTOR_30">
<smd name="VCC@2" x="-29.21" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="A12" x="-26.67" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="A13" x="-24.13" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="A8" x="-21.59" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="A9" x="-19.05" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="A11" x="-16.51" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="A2" x="-13.97" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="A1" x="-11.43" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="D7" x="-8.89" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="D6" x="-6.35" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="D5" x="-3.81" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="D4" x="-1.27" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="CS4" x="1.27" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="CS2" x="3.81" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="GND@2" x="6.35" y="6.35" dx="10.16" dy="1.9304" layer="1" rot="R90"/>
<smd name="VCC@1" x="-29.21" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="A7" x="-26.67" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="A6" x="-24.13" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="A5" x="-21.59" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="A4" x="-19.05" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="A3" x="-16.51" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="A10" x="-13.97" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="A0" x="-11.43" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="D0" x="-8.89" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="D1" x="-6.35" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="D2" x="-3.81" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="D3" x="-1.27" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="CS3" x="1.27" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="CS1" x="3.81" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
<smd name="GND@1" x="6.35" y="6.35" dx="10.16" dy="1.9304" layer="16" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="BUS30">
<pin name="A0" x="-71.12" y="20.32" length="short" rot="R90"/>
<pin name="A1" x="-68.58" y="20.32" length="short" rot="R90"/>
<pin name="A3" x="-63.5" y="20.32" length="short" rot="R90"/>
<pin name="A4" x="-60.96" y="20.32" length="short" rot="R90"/>
<pin name="A5" x="-58.42" y="20.32" length="short" rot="R90"/>
<pin name="A6" x="-55.88" y="20.32" length="short" rot="R90"/>
<pin name="A7" x="-53.34" y="20.32" length="short" rot="R90"/>
<pin name="A8" x="-50.8" y="20.32" length="short" rot="R90"/>
<pin name="A9" x="-48.26" y="20.32" length="short" rot="R90"/>
<pin name="A10" x="-45.72" y="20.32" length="short" rot="R90"/>
<pin name="A11" x="-43.18" y="20.32" length="short" rot="R90"/>
<pin name="A12" x="-40.64" y="20.32" length="short" rot="R90"/>
<pin name="A13" x="-38.1" y="20.32" length="short" rot="R90"/>
<pin name="VCC" x="-76.2" y="20.32" length="short" rot="R90"/>
<pin name="VCC(2)" x="-73.66" y="20.32" length="short" rot="R90"/>
<pin name="GND" x="-5.08" y="20.32" length="short" rot="R90"/>
<pin name="GND(2)" x="-2.54" y="20.32" length="short" rot="R90"/>
<pin name="D0" x="-25.4" y="20.32" length="short" rot="R90"/>
<pin name="D1" x="-22.86" y="20.32" length="short" rot="R90"/>
<pin name="D2" x="-20.32" y="20.32" length="short" rot="R90"/>
<pin name="D3" x="-17.78" y="20.32" length="short" rot="R90"/>
<pin name="D4" x="-15.24" y="20.32" length="short" rot="R90"/>
<pin name="D5" x="-12.7" y="20.32" length="short" rot="R90"/>
<pin name="D6" x="-10.16" y="20.32" length="short" rot="R90"/>
<pin name="D7" x="-7.62" y="20.32" length="short" rot="R90"/>
<pin name="CS1" x="-35.56" y="20.32" length="short" rot="R90"/>
<pin name="CS2" x="-33.02" y="20.32" length="short" rot="R90"/>
<pin name="CS3" x="-30.48" y="20.32" length="short" rot="R90"/>
<pin name="CS4" x="-27.94" y="20.32" length="short" rot="R90"/>
<pin name="A2" x="-66.04" y="20.32" length="short" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SVI_CARTRIDGE" prefix="EDGE">
<gates>
<gate name="G$1" symbol="BUS30" x="-7.62" y="20.32"/>
</gates>
<devices>
<device name="" package="EDGE_CONNECTOR_30">
<connects>
<connect gate="G$1" pin="A0" pad="A0"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A10" pad="A10"/>
<connect gate="G$1" pin="A11" pad="A11"/>
<connect gate="G$1" pin="A12" pad="A12"/>
<connect gate="G$1" pin="A13" pad="A13"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="A7" pad="A7"/>
<connect gate="G$1" pin="A8" pad="A8"/>
<connect gate="G$1" pin="A9" pad="A9"/>
<connect gate="G$1" pin="CS1" pad="CS1"/>
<connect gate="G$1" pin="CS2" pad="CS2"/>
<connect gate="G$1" pin="CS3" pad="CS3"/>
<connect gate="G$1" pin="CS4" pad="CS4"/>
<connect gate="G$1" pin="D0" pad="D0"/>
<connect gate="G$1" pin="D1" pad="D1"/>
<connect gate="G$1" pin="D2" pad="D2"/>
<connect gate="G$1" pin="D3" pad="D3"/>
<connect gate="G$1" pin="D4" pad="D4"/>
<connect gate="G$1" pin="D5" pad="D5"/>
<connect gate="G$1" pin="D6" pad="D6"/>
<connect gate="G$1" pin="D7" pad="D7"/>
<connect gate="G$1" pin="GND" pad="GND@1"/>
<connect gate="G$1" pin="GND(2)" pad="GND@2"/>
<connect gate="G$1" pin="VCC" pad="VCC@1"/>
<connect gate="G$1" pin="VCC(2)" pad="VCC@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="memory-nec">
<description>&lt;b&gt;NEC Memories&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL32">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="20.32" y1="6.731" x2="-20.32" y2="6.731" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-6.731" x2="20.32" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="20.32" y1="6.731" x2="20.32" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="6.731" x2="-20.32" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-6.731" x2="-20.32" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.016" x2="-20.32" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-19.05" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-16.51" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-3.81" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="-1.27" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-13.97" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-11.43" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-6.35" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-8.89" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="1.27" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="3.81" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="6.35" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="8.89" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="11.43" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="13.97" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="16.51" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="19.05" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="19.05" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="16.51" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="13.97" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="11.43" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="8.89" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="6.35" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="3.81" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="1.27" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="25" x="-1.27" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="26" x="-3.81" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="27" x="-6.35" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="28" x="-8.89" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="29" x="-11.43" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="30" x="-13.97" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="31" x="-16.51" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="32" x="-19.05" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<text x="-20.701" y="-6.604" size="1.778" layer="25" rot="R90">&gt;NAME</text>
<text x="-16.891" y="-0.889" size="1.778" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="27C1000A">
<wire x1="-7.62" y1="-35.56" x2="10.16" y2="-35.56" width="0.4064" layer="94"/>
<wire x1="10.16" y1="22.86" x2="10.16" y2="-35.56" width="0.4064" layer="94"/>
<wire x1="10.16" y1="22.86" x2="-7.62" y2="22.86" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="-35.56" x2="-7.62" y2="22.86" width="0.4064" layer="94"/>
<text x="-7.62" y="23.495" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-38.1" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A0" x="-12.7" y="20.32" length="middle" direction="in"/>
<pin name="A1" x="-12.7" y="17.78" length="middle" direction="in"/>
<pin name="A2" x="-12.7" y="15.24" length="middle" direction="in"/>
<pin name="A3" x="-12.7" y="12.7" length="middle" direction="in"/>
<pin name="A4" x="-12.7" y="10.16" length="middle" direction="in"/>
<pin name="A5" x="-12.7" y="7.62" length="middle" direction="in"/>
<pin name="A6" x="-12.7" y="5.08" length="middle" direction="in"/>
<pin name="A7" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="A8" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="A9" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="A10" x="-12.7" y="-5.08" length="middle" direction="in"/>
<pin name="A11" x="-12.7" y="-7.62" length="middle" direction="in"/>
<pin name="A12" x="-12.7" y="-10.16" length="middle" direction="in"/>
<pin name="A13" x="-12.7" y="-12.7" length="middle" direction="in"/>
<pin name="A14" x="-12.7" y="-15.24" length="middle" direction="in"/>
<pin name="A15" x="-12.7" y="-17.78" length="middle" direction="in"/>
<pin name="!PGM" x="-12.7" y="-25.4" length="middle" direction="in"/>
<pin name="!OE" x="-12.7" y="-30.48" length="middle" direction="in"/>
<pin name="!CE" x="-12.7" y="-33.02" length="middle" direction="in"/>
<pin name="O0" x="15.24" y="20.32" length="middle" direction="hiz" rot="R180"/>
<pin name="O1" x="15.24" y="17.78" length="middle" direction="hiz" rot="R180"/>
<pin name="O2" x="15.24" y="15.24" length="middle" direction="hiz" rot="R180"/>
<pin name="O3" x="15.24" y="12.7" length="middle" direction="hiz" rot="R180"/>
<pin name="O4" x="15.24" y="10.16" length="middle" direction="hiz" rot="R180"/>
<pin name="O5" x="15.24" y="7.62" length="middle" direction="hiz" rot="R180"/>
<pin name="O6" x="15.24" y="5.08" length="middle" direction="hiz" rot="R180"/>
<pin name="O7" x="15.24" y="2.54" length="middle" direction="hiz" rot="R180"/>
<pin name="NC" x="15.24" y="-5.08" length="middle" direction="nc" rot="R180"/>
<pin name="GND" x="15.24" y="-33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="VCC" x="15.24" y="-22.86" length="middle" direction="pwr" rot="R180"/>
<pin name="VPP" x="15.24" y="-12.7" length="middle" direction="pas" rot="R180"/>
<pin name="A16" x="-12.7" y="-20.32" length="middle" direction="in"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="27C1001A" prefix="IC" uservalue="yes">
<description>&lt;b&gt;MEMORY&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="27C1000A" x="-2.54" y="5.08"/>
</gates>
<devices>
<device name="" package="DIL32">
<connects>
<connect gate="G$1" pin="!CE" pad="22"/>
<connect gate="G$1" pin="!OE" pad="24"/>
<connect gate="G$1" pin="!PGM" pad="31"/>
<connect gate="G$1" pin="A0" pad="12"/>
<connect gate="G$1" pin="A1" pad="11"/>
<connect gate="G$1" pin="A10" pad="23"/>
<connect gate="G$1" pin="A11" pad="25"/>
<connect gate="G$1" pin="A12" pad="4"/>
<connect gate="G$1" pin="A13" pad="28"/>
<connect gate="G$1" pin="A14" pad="29"/>
<connect gate="G$1" pin="A15" pad="3"/>
<connect gate="G$1" pin="A16" pad="2"/>
<connect gate="G$1" pin="A2" pad="10"/>
<connect gate="G$1" pin="A3" pad="9"/>
<connect gate="G$1" pin="A4" pad="8"/>
<connect gate="G$1" pin="A5" pad="7"/>
<connect gate="G$1" pin="A6" pad="6"/>
<connect gate="G$1" pin="A7" pad="5"/>
<connect gate="G$1" pin="A8" pad="27"/>
<connect gate="G$1" pin="A9" pad="26"/>
<connect gate="G$1" pin="GND" pad="16"/>
<connect gate="G$1" pin="NC" pad="30"/>
<connect gate="G$1" pin="O0" pad="13"/>
<connect gate="G$1" pin="O1" pad="14"/>
<connect gate="G$1" pin="O2" pad="15"/>
<connect gate="G$1" pin="O3" pad="17"/>
<connect gate="G$1" pin="O4" pad="18"/>
<connect gate="G$1" pin="O5" pad="19"/>
<connect gate="G$1" pin="O6" pad="20"/>
<connect gate="G$1" pin="O7" pad="21"/>
<connect gate="G$1" pin="VCC" pad="32"/>
<connect gate="G$1" pin="VPP" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="M27C1001-15F1" constant="no"/>
<attribute name="OC_FARNELL" value="1661756" constant="no"/>
<attribute name="OC_NEWARK" value="78M7615" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VCC">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pal">
<description>&lt;b&gt;PALs&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL20">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="12.7" y1="2.921" x2="-12.7" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-2.921" x2="12.7" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="12.7" y1="2.921" x2="12.7" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="2.921" x2="-12.7" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-2.921" x2="-12.7" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.016" x2="-12.7" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="-11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-13.081" y="-3.048" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-9.779" y="-0.381" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="16V8">
<wire x1="-7.62" y1="15.24" x2="7.62" y2="15.24" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="7.62" y2="15.24" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="-7.62" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="15.24" x2="-7.62" y2="-15.24" width="0.4064" layer="94"/>
<text x="-7.62" y="15.875" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="I0" x="-12.7" y="7.62" length="middle" direction="in"/>
<pin name="I1" x="-12.7" y="5.08" length="middle" direction="in"/>
<pin name="I2" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="I3" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="I4" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="I5" x="-12.7" y="-5.08" length="middle" direction="in"/>
<pin name="I6" x="-12.7" y="-7.62" length="middle" direction="in"/>
<pin name="I7" x="-12.7" y="-10.16" length="middle" direction="in"/>
<pin name="OE/I8" x="-12.7" y="-12.7" length="middle" direction="in" function="dot"/>
<pin name="O1" x="12.7" y="5.08" length="middle" function="dot" rot="R180"/>
<pin name="O2" x="12.7" y="2.54" length="middle" function="dot" rot="R180"/>
<pin name="O3" x="12.7" y="0" length="middle" function="dot" rot="R180"/>
<pin name="O4" x="12.7" y="-2.54" length="middle" function="dot" rot="R180"/>
<pin name="O5" x="12.7" y="-5.08" length="middle" function="dot" rot="R180"/>
<pin name="O6" x="12.7" y="-7.62" length="middle" function="dot" rot="R180"/>
<pin name="O7" x="12.7" y="-10.16" length="middle" function="dot" rot="R180"/>
<pin name="O0" x="12.7" y="7.62" length="middle" function="dot" rot="R180"/>
<pin name="CLK" x="-12.7" y="12.7" length="middle" direction="in" function="clk"/>
</symbol>
<symbol name="PWRN">
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.667" size="1.27" layer="95" rot="R90">VCC</text>
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="16V8" prefix="IC" uservalue="yes">
<description>&lt;b&gt;PAL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="16V8" x="0" y="0"/>
<gate name="P" symbol="PWRN" x="-30.48" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="DIL20">
<connects>
<connect gate="G$1" pin="CLK" pad="1"/>
<connect gate="G$1" pin="I0" pad="2"/>
<connect gate="G$1" pin="I1" pad="3"/>
<connect gate="G$1" pin="I2" pad="4"/>
<connect gate="G$1" pin="I3" pad="5"/>
<connect gate="G$1" pin="I4" pad="6"/>
<connect gate="G$1" pin="I5" pad="7"/>
<connect gate="G$1" pin="I6" pad="8"/>
<connect gate="G$1" pin="I7" pad="9"/>
<connect gate="G$1" pin="O0" pad="12"/>
<connect gate="G$1" pin="O1" pad="13"/>
<connect gate="G$1" pin="O2" pad="14"/>
<connect gate="G$1" pin="O3" pad="15"/>
<connect gate="G$1" pin="O4" pad="16"/>
<connect gate="G$1" pin="O5" pad="17"/>
<connect gate="G$1" pin="O6" pad="18"/>
<connect gate="G$1" pin="O7" pad="19"/>
<connect gate="G$1" pin="OE/I8" pad="11"/>
<connect gate="P" pin="GND" pad="10"/>
<connect gate="P" pin="VCC" pad="20"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="GAL16V8D-7LPN" constant="no"/>
<attribute name="OC_FARNELL" value="9699740" constant="no"/>
<attribute name="OC_NEWARK" value="97K0449" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA04-2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<text x="-4.318" y="-4.191" size="1.27" layer="21" ratio="10">1</text>
<text x="-5.08" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="3.81" y="2.921" size="1.27" layer="21" ratio="10">8</text>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MA04-2">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA04-2" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA04-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA04-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
<clearance class="0" value="0.205"/>
</class>
</classes>
<parts>
<part name="EDGE1" library="svi_cartridge" deviceset="SVI_CARTRIDGE" device=""/>
<part name="IC1" library="memory-nec" deviceset="27C1001A" device=""/>
<part name="P+1" library="supply1" deviceset="VCC" device=""/>
<part name="P+2" library="supply1" deviceset="VCC" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="IC2" library="pal" deviceset="16V8" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="SV1" library="con-lstb" deviceset="MA04-2" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="EDGE1" gate="G$1" x="-33.02" y="0" rot="R270"/>
<instance part="IC1" gate="G$1" x="78.74" y="50.8"/>
<instance part="P+1" gate="VCC" x="-12.7" y="83.82"/>
<instance part="P+2" gate="VCC" x="99.06" y="35.56"/>
<instance part="GND1" gate="1" x="0" y="-10.16"/>
<instance part="GND2" gate="1" x="104.14" y="10.16"/>
<instance part="IC2" gate="G$1" x="43.18" y="27.94"/>
<instance part="GND5" gate="1" x="10.16" y="7.62"/>
<instance part="SV1" gate="G$1" x="17.78" y="22.86"/>
</instances>
<busses>
<bus name="A[0..13]">
<segment>
<wire x1="55.88" y1="38.1" x2="55.88" y2="71.12" width="0.762" layer="92"/>
<wire x1="55.88" y1="71.12" x2="5.08" y2="71.12" width="0.762" layer="92"/>
<wire x1="5.08" y1="71.12" x2="5.08" y2="38.1" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="D[0..7]">
<segment>
<wire x1="5.08" y1="25.4" x2="5.08" y2="0" width="0.762" layer="92"/>
<wire x1="5.08" y1="0" x2="111.76" y2="0" width="0.762" layer="92"/>
<wire x1="111.76" y1="0" x2="111.76" y2="71.12" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="A13" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="A13"/>
<wire x1="5.08" y1="38.1" x2="-12.7" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="55.88" y1="38.1" x2="66.04" y2="38.1" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A13"/>
</segment>
</net>
<net name="A12" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="A12"/>
<wire x1="5.08" y1="40.64" x2="-12.7" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="55.88" y1="40.64" x2="66.04" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A12"/>
</segment>
</net>
<net name="A11" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="A11"/>
<wire x1="5.08" y1="43.18" x2="-12.7" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="A11"/>
<wire x1="55.88" y1="43.18" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A10" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="A10"/>
<wire x1="5.08" y1="45.72" x2="-12.7" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="55.88" y1="45.72" x2="66.04" y2="45.72" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A10"/>
</segment>
</net>
<net name="A9" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="A9"/>
<wire x1="5.08" y1="48.26" x2="-12.7" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="55.88" y1="48.26" x2="66.04" y2="48.26" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A9"/>
</segment>
</net>
<net name="A8" class="0">
<segment>
<wire x1="55.88" y1="50.8" x2="66.04" y2="50.8" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A8"/>
</segment>
<segment>
<pinref part="EDGE1" gate="G$1" pin="A8"/>
<wire x1="5.08" y1="50.8" x2="-12.7" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<wire x1="55.88" y1="53.34" x2="66.04" y2="53.34" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A7"/>
</segment>
<segment>
<pinref part="EDGE1" gate="G$1" pin="A7"/>
<wire x1="5.08" y1="53.34" x2="-12.7" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<wire x1="55.88" y1="55.88" x2="66.04" y2="55.88" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A6"/>
</segment>
<segment>
<pinref part="EDGE1" gate="G$1" pin="A6"/>
<wire x1="5.08" y1="55.88" x2="-12.7" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<wire x1="55.88" y1="58.42" x2="66.04" y2="58.42" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A5"/>
</segment>
<segment>
<pinref part="EDGE1" gate="G$1" pin="A5"/>
<wire x1="5.08" y1="58.42" x2="-12.7" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<wire x1="55.88" y1="60.96" x2="66.04" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A4"/>
</segment>
<segment>
<pinref part="EDGE1" gate="G$1" pin="A4"/>
<wire x1="5.08" y1="60.96" x2="-12.7" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="A3"/>
<wire x1="66.04" y1="63.5" x2="55.88" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="EDGE1" gate="G$1" pin="A3"/>
<wire x1="5.08" y1="63.5" x2="-12.7" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<wire x1="55.88" y1="66.04" x2="66.04" y2="66.04" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A2"/>
</segment>
<segment>
<pinref part="EDGE1" gate="G$1" pin="A2"/>
<wire x1="5.08" y1="66.04" x2="-12.7" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<wire x1="55.88" y1="68.58" x2="66.04" y2="68.58" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A1"/>
</segment>
<segment>
<pinref part="EDGE1" gate="G$1" pin="A1"/>
<wire x1="5.08" y1="68.58" x2="-12.7" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="A0"/>
<wire x1="66.04" y1="71.12" x2="55.88" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="EDGE1" gate="G$1" pin="A0"/>
<wire x1="5.08" y1="71.12" x2="-12.7" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<wire x1="93.98" y1="27.94" x2="99.06" y2="27.94" width="0.1524" layer="91"/>
<wire x1="99.06" y1="27.94" x2="99.06" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<pinref part="EDGE1" gate="G$1" pin="VCC"/>
<wire x1="-12.7" y1="81.28" x2="-12.7" y2="76.2" width="0.1524" layer="91"/>
<pinref part="EDGE1" gate="G$1" pin="VCC(2)"/>
<wire x1="-12.7" y1="76.2" x2="-12.7" y2="73.66" width="0.1524" layer="91"/>
<junction x="-12.7" y="76.2"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="104.14" y1="12.7" x2="104.14" y2="17.78" width="0.1524" layer="91"/>
<wire x1="104.14" y1="17.78" x2="93.98" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="104.14" y1="17.78" x2="104.14" y2="38.1" width="0.1524" layer="91"/>
<junction x="104.14" y="17.78"/>
<pinref part="IC1" gate="G$1" pin="VPP"/>
<wire x1="104.14" y1="38.1" x2="93.98" y2="38.1" width="0.1524" layer="91"/>
<wire x1="104.14" y1="38.1" x2="104.14" y2="45.72" width="0.1524" layer="91"/>
<junction x="104.14" y="38.1"/>
<pinref part="IC1" gate="G$1" pin="NC"/>
<wire x1="104.14" y1="45.72" x2="93.98" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="8"/>
<pinref part="SV1" gate="G$1" pin="6"/>
<wire x1="10.16" y1="25.4" x2="10.16" y2="22.86" width="0.1524" layer="91"/>
<pinref part="SV1" gate="G$1" pin="4"/>
<wire x1="10.16" y1="22.86" x2="10.16" y2="20.32" width="0.1524" layer="91"/>
<junction x="10.16" y="22.86"/>
<pinref part="SV1" gate="G$1" pin="2"/>
<wire x1="10.16" y1="20.32" x2="10.16" y2="17.78" width="0.1524" layer="91"/>
<junction x="10.16" y="20.32"/>
<junction x="10.16" y="17.78"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="OE/I8"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="17.78" width="0.1524" layer="91"/>
<wire x1="30.48" y1="15.24" x2="10.16" y2="15.24" width="0.1524" layer="91"/>
<junction x="10.16" y="15.24"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="0" y1="-7.62" x2="0" y2="2.54" width="0.1524" layer="91"/>
<pinref part="EDGE1" gate="G$1" pin="GND"/>
<wire x1="0" y1="2.54" x2="0" y2="5.08" width="0.1524" layer="91"/>
<wire x1="0" y1="5.08" x2="-12.7" y2="5.08" width="0.1524" layer="91"/>
<pinref part="EDGE1" gate="G$1" pin="GND(2)"/>
<wire x1="0" y1="2.54" x2="-12.7" y2="2.54" width="0.1524" layer="91"/>
<junction x="0" y="2.54"/>
</segment>
</net>
<net name="D7" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="D7"/>
<wire x1="5.08" y1="7.62" x2="-12.7" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="O7"/>
<wire x1="111.76" y1="53.34" x2="93.98" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="D6"/>
<wire x1="5.08" y1="10.16" x2="-12.7" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="O6"/>
<wire x1="111.76" y1="55.88" x2="93.98" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="D5"/>
<wire x1="5.08" y1="12.7" x2="-12.7" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="O5"/>
<wire x1="111.76" y1="58.42" x2="93.98" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D4" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="D4"/>
<wire x1="5.08" y1="15.24" x2="-12.7" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="O4"/>
<wire x1="111.76" y1="60.96" x2="93.98" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="D3"/>
<wire x1="5.08" y1="17.78" x2="-12.7" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="O3"/>
<wire x1="111.76" y1="63.5" x2="93.98" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="D2"/>
<wire x1="5.08" y1="20.32" x2="-12.7" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="O2"/>
<wire x1="111.76" y1="66.04" x2="93.98" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D1" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="D1"/>
<wire x1="5.08" y1="22.86" x2="-12.7" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="O1"/>
<wire x1="111.76" y1="68.58" x2="93.98" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D0" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="D0"/>
<wire x1="5.08" y1="25.4" x2="-12.7" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="O0"/>
<wire x1="111.76" y1="71.12" x2="93.98" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="O0"/>
<wire x1="55.88" y1="35.56" x2="66.04" y2="35.56" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A14"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="O1"/>
<wire x1="55.88" y1="33.02" x2="66.04" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A15"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="CS1"/>
<wire x1="-12.7" y1="35.56" x2="30.48" y2="35.56" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="I0"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="CS2"/>
<wire x1="-12.7" y1="33.02" x2="30.48" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="I1"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="CS3"/>
<wire x1="-12.7" y1="30.48" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="I2"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="EDGE1" gate="G$1" pin="CS4"/>
<wire x1="-12.7" y1="27.94" x2="30.48" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="I3"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="O2"/>
<wire x1="55.88" y1="30.48" x2="66.04" y2="30.48" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A16"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="SV1" gate="G$1" pin="1"/>
<pinref part="IC2" gate="G$1" pin="I7"/>
<wire x1="25.4" y1="17.78" x2="30.48" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="SV1" gate="G$1" pin="7"/>
<pinref part="IC2" gate="G$1" pin="I4"/>
<wire x1="25.4" y1="25.4" x2="30.48" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="O3"/>
<pinref part="IC1" gate="G$1" pin="!PGM"/>
<wire x1="55.88" y1="27.94" x2="60.96" y2="25.4" width="0.1524" layer="91"/>
<wire x1="60.96" y1="25.4" x2="66.04" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="O4"/>
<pinref part="IC1" gate="G$1" pin="!OE"/>
<wire x1="55.88" y1="25.4" x2="60.96" y2="20.32" width="0.1524" layer="91"/>
<wire x1="60.96" y1="20.32" x2="66.04" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="O5"/>
<wire x1="55.88" y1="22.86" x2="60.96" y2="17.78" width="0.1524" layer="91"/>
<wire x1="60.96" y1="17.78" x2="66.04" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="!CE"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="I6"/>
<pinref part="SV1" gate="G$1" pin="3"/>
<wire x1="30.48" y1="20.32" x2="25.4" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="I5"/>
<pinref part="SV1" gate="G$1" pin="5"/>
<wire x1="30.48" y1="22.86" x2="25.4" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
